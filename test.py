from typing import Dict, List, TypeVar, Union


def test_list():
    a: List
    # Hint + restriction
    _ = a.asd


def test_inherit():
    class A(object):
        a: int

    class B(A):
        b: float
        c: A

    a: B
    # Type hinting and restriction
    _ = a.asd
    _ = a.c.asd


def test_type_var():
    A = TypeVar("A", int)
    a: A = None
    # No hint
    # TypeVar too generic
    _ = a.asd


def test_union():
    a: Union[int, str]
    # Hint + restriction
    _ = a.asd


def test_subclass():
    class A(List[int]):
        a: int

    a: A
    # Hint + restriction
    # class attributes
    _ = a.a.asd
    # No restriction
    # base class as Any if metaclass
    _ = a.asd


def test_subclass_union1():
    class A(List[int]):
        a: int

    a: Union[A, List[int]]
    # Hint + restriction
    _ = a.a.asd
    _ = a.asd
    # No restriction to list type
    # A shadows List
    _ = a.pop().asd


def test_subclass_union2():
    class A(List[int]):
        a: int

    # List first item
    a: Union[List[int], A]
    # Hint + restriction
    _ = a.a.asd
    _ = a.asd
    _ = a.pop().asd


def test_dict():
    a: Dict[int, float]
    # Hint + restriction
    _ = a.asd
    _ = a[1].asd
    _ = a.get(1).asd
    _ = a.get(1.0).asd
    _ = list(a.values())[0].asd


def test_dict_union():
    class A(dict):
        def values(self) -> List[float]: pass

        def keys(self) -> List[int]: pass

    a: Union[Dict[int, float], A]
    # Hint + restriction
    _ = a.asd
    _ = a[1].asd
    _ = a.get(1).asd
    _ = a.get(1.0).asd
    _ = a.values()[0].asd


def test_dict_union_function():
    def my_dict(kt, vt):
        class Cls(dict):
            def keys(self) -> List[kt]: pass

            def values(self) -> List[vt]: pass

        return Cls

    A = my_dict(int, float)

    a: Union[Dict[int, float], A]
    # Hint + restriction
    _ = a.asd
    _ = a[1].asd
    _ = a.get(1).asd
    _ = a.get(1.0).asd
    # No hint for generated type
    _ = a.values()[0].asd
